#AW Checkout
Made by AfonsoWilsson.

jQuery is required.

*This extension is now a part of https://github.com/afonsowilsson/magento-boilerplate*

##Installation
###With modgit
$ cd /path/to/magento  
$ modgit init  
$ modgit -e README.md -e .gitignore clone awcheckout https://github.com/afonsowilsson/awcheckout.git

###Manual installation
Simply move the files where they belong.
